import os
import sys
sys.path.append('..')
sys.path.append('../..')
import argparse
import utils
from utils import *

def make_output(file, params=[]):
	input_string = ""

	for i in range(20):
		input_string += make_cluster(i)

	input_string += "\n"

	for i in range(20):
		input_string += make_conquered(i)

	write_to_file(file, input_string, False)

def make_cluster(iteration):
	return_output = ""

	return_output += str(10*iteration) + " " + str(10*iteration + 1) + " " + str(10*iteration + 4) + " "
	return_output += str(10*iteration + 5) + " " + str(10*iteration + 6) + " " + str(10*iteration + 7) + " "
	return_output += str(10*iteration + 6) + " " + str(10*iteration + 8) + " " + str(10*iteration + 6) + " "
	return_output += str(10*iteration + 9) + " "

	if iteration == 19:
		return_output += str(0)

	return return_output

def make_conquered(iteration):
	return_output = ""

	return_output += str(10*iteration + 1) + " " + str(10*iteration + 5) + " " + str(10*iteration + 7) + " "
	return_output += str(10*iteration + 8) + " " + str(10*iteration + 9)

	if iteration != 19:
		return_output += " "

	return return_output

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parsing arguments')
    parser.add_argument('--all', action='store_true', help='If specified, the input validator is run on all files in the input directory. Else, it is run on just the given input file')
    parser.add_argument('input', type=str, help='The path to the input file or directory')
    parser.add_argument('params', nargs=argparse.REMAINDER, help='Extra arguments passed in')
    args = parser.parse_args()
    if args.all:
        input_directory = args.input
        make_output(input_directory, params=args.params)
    else:
        input_file = args.input
        make_output(input_file, params=args.params)