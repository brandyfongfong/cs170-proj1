import os
import sys
import numpy as np
sys.path.append('..')
sys.path.append('../..')
import argparse
import utils
from utils import *
k = 50

def kingdom_generate(file, params=[]):
    vertices = np.random.normal(0,25,(k,k))
    distances = np.empty([k,k])
    for x in range(0, k):
        for y in range(0, k):
            if (x == y):
                distances[x][y] = round(np.random.random() * 20, 2)
            elif (np.random.random() < 0.5):
                val = round(np.linalg.norm(vertices[x] - vertices[y]), 2)
                if (val < 0.1):
                    val = 0
                distances[x][y] = val
                distances[y][x] = val
            else:
                distances[x][y] = 0
                distances[y][x] = 0

    return_string = ""

    return_string += str(k) + "\n"

    for i in range(k - 1):
        return_string = return_string + str(i) + " "

    return_string = return_string + str(k - 1) + "\n" + str(0) + "\n"
    
    for x in range(k):
        line = ""
        row = distances[x]

        for y in range(k):
            elem = row[y]

            if elem == 0.0:
                elem = "x"

            if y == k - 1:
                line += str(elem)
            else:
                line += str(elem) + " "

        if x == k - 1:
            return_string += line
        else:
            return_string += line + "\n"

    write_to_file(file, return_string, False)
                
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parsing arguments')
    parser.add_argument('--all', action='store_true', help='If specified, the input validator is run on all files in the input directory. Else, it is run on just the given input file')
    parser.add_argument('input', type=str, help='The path to the input file or directory')
    parser.add_argument('params', nargs=argparse.REMAINDER, help='Extra arguments passed in')
    args = parser.parse_args()
    if args.all:
        input_directory = args.input
        kingdom_generate(input_directory, params=args.params)
    else:
        input_file = args.input
        kingdom_generate(input_file, params=args.params)
