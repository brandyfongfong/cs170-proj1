import os
import sys
sys.path.append('..')
sys.path.append('../..')
import argparse
import utils
from utils import *

def make_input(file, params=[]):
	input_string = "200\n"

	for i in range(199):
		input_string = input_string + str(i) + " "

	input_string = input_string + str(199) + "\n" + str(0) + "\n"

	for i in range(20):
		input_string += make_cluster(i)

	write_to_file(file, input_string, False)

def make_cluster(iteration):
	return_input = ""

	if iteration == 0:
		return_input += line_maker(10 * iteration, "1 1", 197 - 10 * iteration) + " 1" + "\n"
	else:
		return_input += line_maker(10 * iteration - 1, "1 1 1", 197 - 10 * iteration + 1) + "\n"

	return_input += line_maker(10 * iteration, "1 1 1 1 1", 195 - 10 * iteration) + "\n"
	return_input += line_maker(10 * iteration + 1, "1 1", 198 - 10 * iteration - 1) + "\n"
	return_input += line_maker(10 * iteration + 1, "1 x 1", 197 - 10 * iteration - 1) + "\n"
	return_input += line_maker(10 * iteration + 1, "1 x x 1 1", 195 - 10 * iteration - 1) + "\n"

	return_input += line_maker(10 * iteration + 4, "1 1 1", 197 - 10 * iteration - 4) + "\n"
	return_input += line_maker(10 * iteration + 5, "1 25 1 1 1", 195 - 10 * iteration - 5) + "\n"
	return_input += line_maker(10 * iteration + 6, "1 1", 198 - 10 * iteration - 6) + "\n"
	return_input += line_maker(10 * iteration + 6, "1 x 1", 197 - 10 * iteration - 6) + "\n"

	if iteration == 19:
		return_input += "1 " + line_maker(10 * iteration + 5, "1 x x 1", 0)
	else:
		return_input += line_maker(10 * iteration + 6, "1 x x 1 1", 195 - 10 * iteration - 6) + "\n"

	return return_input

def line_maker(before, defined, after):
	return "x " * before + defined + " x" * after

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parsing arguments')
    parser.add_argument('--all', action='store_true', help='If specified, the input validator is run on all files in the input directory. Else, it is run on just the given input file')
    parser.add_argument('input', type=str, help='The path to the input file or directory')
    parser.add_argument('params', nargs=argparse.REMAINDER, help='Extra arguments passed in')
    args = parser.parse_args()
    if args.all:
        input_directory = args.input
        make_input(input_directory, params=args.params)
    else:
        input_file = args.input
        make_input(input_file, params=args.params)