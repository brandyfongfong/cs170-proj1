import os
import sys
sys.path.append('..')
sys.path.append('../..')
import argparse
import utils
from utils import *
from student_utils_sp18 import *

def solve(list_of_kingdom_names, starting_kingdom, adjacency_matrix, params=[]):
    """
    Write your algorithm here.
    Input:
        list_of_kingdom_names: An list of kingdom names such that node i of the graph corresponds to name index i in the list
        starting_kingdom: The name of the starting kingdom for the walk
        adjacency_matrix: The adjacency matrix from the input file

    Output:
        Return 2 things. The first is a list of kingdoms representing the walk, and the second is the set of kingdoms that are conquered
    """

    # This import statement didn't work outside of the function for some reason.
    from scipy.sparse.csgraph import shortest_path

    # Initialized variables
    original_index = list_of_kingdom_names.index(starting_kingdom)
    num_kingdoms = len(list_of_kingdom_names)
    all_kingdoms = set(list_of_kingdom_names)
    seen = set()
    conquered = []
    walk = [starting_kingdom]

    # Create matrix of just distances without conquer costs.
    distances_only = []

    for i in range(num_kingdoms):
        new_row = []

        for j in range(num_kingdoms):
            if i == j or adjacency_matrix[i][j] == 'x':
                new_row += [0]
            else:
                new_row += [adjacency_matrix[i][j]]

        distances_only += [new_row]

    # Create Dijkstra distance matrix.
    dijkstra_matrix, predecessors = shortest_path(csgraph=distances_only, method='D', return_predecessors=True)

    # Prep for iteration.
    current_index = original_index
    current_dijkstras = dijkstra_matrix[current_index]

    # Iterate.
    while len(seen) < num_kingdoms:
        # Create local variables.
        max_value = float('-inf')
        max_index = None

        # Iterate through all unconquered kingdoms.
        for kingdom in all_kingdoms.difference(set(conquered)):
            # Index of current kingdom being checked.
            check_index = list_of_kingdom_names.index(kingdom)

            # Adjacency row of current kingdom being checked.
            check_row = adjacency_matrix[check_index]

            # Distance to current kingdom being checked from current kingdom at.
            distance_to = current_dijkstras[check_index]

            # Cost to conquer current kingdom being checked.
            cost_to_conquer = check_row[check_index]

            # Value of conquering current kingdom being checked.
            check_value = -cost_to_conquer - distance_to

            # Iterate through all adjacent kingdoms to the current kingdom being checked.
            for i in range(num_kingdoms):
                if check_row[i] != 'x' and i != check_index and list_of_kingdom_names[i] not in seen:
                    check_value += float(check_row[i]) + float(adjacency_matrix[i][i])

            # Update if higher value than current max value.
            if max_value < check_value:
                max_value = check_value
                max_index = check_index

        # Add shortest path from current kinngdom to conquered kingdom.
        current_predecessors_row = predecessors[current_index]
        shortest_path = []
        sp_index = max_index

        while sp_index != current_index:
            shortest_path = [list_of_kingdom_names[sp_index]] + shortest_path
            sp_index = current_predecessors_row[sp_index]

        walk += shortest_path

        # Add to conquered and seen set.
        conquered.append(list_of_kingdom_names[max_index]) 
        seen.add(list_of_kingdom_names[max_index])

        # Update seen set with adjacent kingdoms.
        check_row = adjacency_matrix[max_index]

        for i in range(num_kingdoms):
            if check_row[i] != 'x':
                seen.add(list_of_kingdom_names[i])

        # Set up for next iteration
        current_index = max_index
        current_dijkstras = dijkstra_matrix[max_index]

    # Complete walk.
    shortest_path = []
    current_predecessors_row = predecessors[current_index]
    sp_index = original_index

    while sp_index != current_index:
        shortest_path = [list_of_kingdom_names[sp_index]] + shortest_path
        sp_index = current_predecessors_row[sp_index]

    walk += shortest_path

    return walk, conquered


"""
======================================================================
   No need to change any code below this line
======================================================================
"""


def solve_from_file(input_file, output_directory, params=[]):
    print('Processing', input_file)
    
    input_data = utils.read_file(input_file)
    number_of_kingdoms, list_of_kingdom_names, starting_kingdom, adjacency_matrix = data_parser(input_data)
    closed_walk, conquered_kingdoms = solve(list_of_kingdom_names, starting_kingdom, adjacency_matrix, params=params)

    basename, filename = os.path.split(input_file)
    output_filename = utils.input_to_output(filename)
    output_file = f'{output_directory}/{output_filename}'
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    utils.write_data_to_file(output_file, closed_walk, ' ')
    utils.write_to_file(output_file, '\n', append=True)
    utils.write_data_to_file(output_file, conquered_kingdoms, ' ', append=True)


def solve_all(input_directory, output_directory, params=[]):
    input_files = utils.get_files_with_extension(input_directory, 'in')

    for input_file in input_files:
        solve_from_file(input_file, output_directory, params=params)


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Parsing arguments')
    parser.add_argument('--all', action='store_true', help='If specified, the solver is run on all files in the input directory. Else, it is run on just the given input file')
    parser.add_argument('input', type=str, help='The path to the input file or directory')
    parser.add_argument('output_directory', type=str, nargs='?', default='.', help='The path to the directory where the output should be written')
    parser.add_argument('params', nargs=argparse.REMAINDER, help='Extra arguments passed in')
    args = parser.parse_args()
    output_directory = args.output_directory
    if args.all:
        input_directory = args.input
        solve_all(input_directory, output_directory, params=args.params)
    else:
        input_file = args.input
        solve_from_file(input_file, output_directory, params=args.params)
