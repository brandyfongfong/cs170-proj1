# Existing imports from the first solver.
import math
import sys
sys.path.append('..')
sys.path.append('../..')
import argparse
import utils
from utils import *
from student_utils_sp18 import *
import numpy as np
from scipy.sparse.csgraph import shortest_path

# Import for Google TSP solvers.
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2

class CreateDistanceCallback(object):
  def __init__(self, adjacency_matrix):
    self.matrix = adjacency_matrix


  def Distance(self, from_node, to_node):
    return self.matrix[from_node][to_node]

def solve(list_of_kingdom_names, starting_kingdom, adjacency_matrix, params=[]):
  # Initialize variables.
  num_kingdoms = len(list_of_kingdom_names)                      # Number of kingdoms
  starting_index = list_of_kingdom_names.index(starting_kingdom) # Index of starting kingdom
  all_kingdoms = set(list(range(num_kingdoms)))                  # A set of numbers
  surrendered_kingdoms = set()                                   # Set of kingdoms surrendered/conquered
  kingdoms_to_conquer = set()                                    # Set of kingdoms we wish to conquer
  remove_start_index = False                                     # Assumes that start index was conquered for tsp, but remove if set to True

  # Create return strings.
  walk = [starting_kingdom] # Returned answer of the path
  conquered = []            # Which kingdom was conquered

  # Create adjacency matrix without conquer costs.
  distances_only = [] # Distance matrix without conquer costs formatted for Dijkstra's method

  for i in range(num_kingdoms): # Add rows to distances_only
    new_row = []                # Initialize new row

    for j in range(num_kingdoms):                 # Add kingdom distances to new_row
      if i == j or adjacency_matrix[i][j] == 'x': # If conquered cost or not connected then replace with 0
        new_row += [0]
      else:
        new_row += [adjacency_matrix[i][j]]       # else input cost of edge (distance between kingdoms)

    distances_only += [new_row]                   # Add row to distances_only

  # Create Dijkstra distance matrix.
  dijkstra_matrix, predecessors = shortest_path(csgraph=distances_only, method='D', return_predecessors=True)

  # Initialzied array with initial heuristic values.
  heuristic_values = [] # Heuristic value list containing the gain from conquering kingdom i

  for i in range(num_kingdoms):    # Iterate through all kingdoms
    curr_row = adjacency_matrix[i] # Get kingdom's i's neighbors
    cost_to_conquer = curr_row[i]  # Conquer cost of kingdom i
    curr_value = -cost_to_conquer  # Initialize heuristic of conquering kingdom i

    for j in range(num_kingdoms):  # Iterate through all neighbors
      if curr_row[j] != 'x' and j != i and j:                             # If it is a valid non-surrendered neighbor of i
        curr_value += float(curr_row[j]) + float(adjacency_matrix[j][j])  # Gain from not needing to travel to and conquer neighbor j because of surrendering

    heuristic_values = heuristic_values + [curr_value] # Add heuristic of kingdom i to heuristic_values at index i

  last_index = starting_index # Added for testing purposes. POSSIBLY PERMANENT

  # Select the kingdoms we wish to conquer.
  while len(surrendered_kingdoms) < num_kingdoms: # Ends when all kingdoms have been chosen to be conquered or surrendered
    max_value = float('-inf') # Best heuristic value of the best kingdom we wish to conquer
    max_index = None          # Best kingdom we wish to conquer

    # Best kingdom to conquer next given the heuristic and distance from last_index POSSIBLY PERMANENT for last_index
    for i in all_kingdoms.difference(kingdoms_to_conquer):                 # Iterate through all kingdoms that have not been chosen to be conquered
      if max_value < heuristic_values[i] - dijkstra_matrix[last_index][i]: # If greater value gained from conquering i than best choice
        max_value = heuristic_values[i] - dijkstra_matrix[last_index][i]   # Change kingdom i's value to max_value
        max_index = i                                                      # Change kingdom i to max_index

    # Add to conquered and seen set.
    kingdoms_to_conquer.add(max_index)  # Add best choice to kingdoms we wish to conquer
    surrendered_kingdoms.add(max_index) # Add to kingdoms that will surrender/be conquered

    # Update surrendered_kingdoms set with adjacent surrendering kingdoms and update heuristic values.
    curr_row = adjacency_matrix[max_index] # Get neighboring kingdoms that will surrender

    for i in range(num_kingdoms):   # Iterate through kingdoms
      if curr_row[i] != 'x':        # If kingdom i is a neighbor
        surrendered_kingdoms.add(i) # Add kingdom i which will surrender
        
        heuristic_values[i] -= float(adjacency_matrix[max_index][max_index]) + float(curr_row[i]) # Remove value of max_index surrendering and cost of path

    last_index = max_index # Set last_index used to be the current max_index POSSIBLY PERMANENT

  # Add starting kingdom if it is not being conquered already.
  if starting_index not in kingdoms_to_conquer: # If starting_index is not worthy of conquering
    remove_start_index = True                   # Set to true so that start_index will be removed from conquered list
    kingdoms_to_conquer.add(starting_index)     # Add starting_index temporarily to conquered kingdoms

  # Create new distance adjacency matrix for subset of kingdoms to conquer.
  conquered_kingdoms_adjacency_matrix = []           # Initialize subgraph adjacency list for kingdoms_to_conquer
  kingdoms_to_conquer = list(kingdoms_to_conquer)    # Make kingdoms_to_conquer into a list from a set
  num_kingdoms_to_conquer = len(kingdoms_to_conquer) # Length of kingdoms_to_conquer

  for i in range(num_kingdoms_to_conquer): # Iterate through number of kingdoms_to_conquer per row
    new_row = []                           # Initialize new_row
    first_kingdom = kingdoms_to_conquer[i] # Adjency row of first_kingdom index

    for j in range(num_kingdoms_to_conquer):  # Iterate through number of kingdoms_to_conquer per kingdom
      second_kingdom = kingdoms_to_conquer[j] # Possible neigbor of first_kingdom
      if i == j:       # If kingdom is itself
        new_row += [0] # Conquered cost set to 0
      else:            # else add cost of edge between first and second kingdom
        new_row += [dijkstra_matrix[first_kingdom][second_kingdom]] # Add cost of edge to new_row

    conquered_kingdoms_adjacency_matrix += [new_row] # Add row to adjacency matrix

  tsp_starting_index = kingdoms_to_conquer.index(starting_index)

  # Create TSP order of kingdoms to conquer.
  if num_kingdoms_to_conquer > 0:
    routing = pywrapcp.RoutingModel(num_kingdoms_to_conquer, 1, tsp_starting_index)
    search_parameters = pywrapcp.RoutingModel_DefaultSearchParameters()
    search_parameters.local_search_metaheuristic = (routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH)

    # Set search time limit in milliseconds.
    search_parameters.time_limit_ms = 60000

    # Callback to the distance function. The callback takes two
    # arguments (the from and to node indices) and returns the distance between them.
    dist_between_locations = CreateDistanceCallback(dijkstra_matrix)
    dist_callback = dist_between_locations.Distance
    routing.SetArcCostEvaluatorOfAllVehicles(dist_callback)

    # Solve, returns a solution if any.
    assignment = routing.SolveWithParameters(search_parameters)

    if assignment:
      # For testing purposes: final weight.
      # final_weight = str(assignment.ObjectiveValue())

      # Initialize variables.
      node = routing.Start(0)

      # Create list of conquered kingdoms in order.
      while not routing.IsEnd(node):
        conquered += [kingdoms_to_conquer[node]]
        node = assignment.Value(routing.NextVar(node))

      conquered += [starting_index]

  # Create output arrays.
  for i in range(len(conquered) - 1):
    curr_index, next_index = conquered[i], conquered[i + 1]
    current_predecessors_row = predecessors[curr_index]
    curr_path = []

    while curr_index != next_index:
        curr_path = [list_of_kingdom_names[next_index]] + curr_path
        next_index = current_predecessors_row[next_index]

    walk += curr_path

  for i in range(num_kingdoms_to_conquer):
    conquered[i] = list_of_kingdom_names[conquered[i]]

  del conquered[-1]

  if remove_start_index:
    del conquered[0]

  return walk, conquered

"""
================================================
   No need to change any code below this line
================================================
"""

def solve_from_file(input_file, output_directory, params=[]):
    print('Processing', input_file)
    
    input_data = utils.read_file(input_file)
    number_of_kingdoms, list_of_kingdom_names, starting_kingdom, adjacency_matrix = data_parser(input_data)
    closed_walk, conquered_kingdoms = solve(list_of_kingdom_names, starting_kingdom, adjacency_matrix, params=params)

    basename, filename = os.path.split(input_file)
    output_filename = utils.input_to_output(filename)
    output_file = f'{output_directory}/{output_filename}'
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    utils.write_data_to_file(output_file, closed_walk, ' ')
    utils.write_to_file(output_file, '\n', append=True)
    utils.write_data_to_file(output_file, conquered_kingdoms, ' ', append=True)

def solve_all(input_directory, output_directory, params=[]):
    input_files = utils.get_files_with_extension(input_directory, 'in')

    for input_file in input_files:
        solve_from_file(input_file, output_directory, params=params)

if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Parsing arguments')
    parser.add_argument('--all', action='store_true', help='If specified, the solver is run on all files in the input directory. Else, it is run on just the given input file')
    parser.add_argument('input', type=str, help='The path to the input file or directory')
    parser.add_argument('output_directory', type=str, nargs='?', default='.', help='The path to the directory where the output should be written')
    parser.add_argument('params', nargs=argparse.REMAINDER, help='Extra arguments passed in')
    args = parser.parse_args()
    output_directory = args.output_directory
    if args.all:
        input_directory = args.input
        solve_all(input_directory, output_directory, params=args.params)
    else:
        input_file = args.input
        solve_from_file(input_file, output_directory, params=args.params)
